package Server;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class StarSignsMap {
    public Map<String, Pair<String, String>> sgMap = new HashMap<>();

    public void readFromFile() throws FileNotFoundException {
        File file = new File("src/main/java/Server/starSign.txt");
        Scanner scanner = new Scanner(file);
        String intervalStart, intervalEnd, starSign;

        while (scanner.hasNextLine()) {
            Pair<String, String> interval = new Pair<>(scanner.next() + "/2000", scanner.next() + "/2000");
            starSign = scanner.next();
            sgMap.put(starSign, interval);
            if (scanner.hasNextLine()) {
                scanner.nextLine();
            }
        }
    }

    public String determineStarSign(String givenDate) {
        try {
            readFromFile();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate localDate = LocalDate.parse(givenDate, formatter);
        System.out.println(localDate);

        Set<Map.Entry<String, Pair<String, String>>> setMap = sgMap.entrySet();

        for (Map.Entry<String, Pair<String, String>> me : setMap) {
            if (isStarSign(localDate, me.getKey())) {
                System.out.println("it is what it is, a...");
                System.out.println(me.getKey());
                return me.getKey();
            }
        }
        return "Unknown star sign";
    }

    private boolean isStarSign(LocalDate localDate, String starSign) {
        Pair<String, String> value = new Pair<>(sgMap.get(starSign).getFirst(), sgMap.get(starSign).getSecond());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate localDate1 = LocalDate.parse(value.getFirst(), formatter);
        LocalDate localDate2 = LocalDate.parse(value.getSecond(), formatter);
        if ((localDate.getMonthValue() == localDate2.getMonthValue()) &&
                (localDate.getDayOfMonth() <= localDate2.getDayOfMonth())) {
            return true;
        }
        if ((localDate.getMonthValue() == localDate1.getMonthValue()) &&
                (localDate.getDayOfMonth() >= localDate1.getDayOfMonth())) {
            return true;
        }
        return false;
    }
}