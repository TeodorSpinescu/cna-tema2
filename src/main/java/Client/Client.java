package Client;

import io.grpc.Channel;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import resouces.protos.zodiac.StarSignServiceGrpc;
import resouces.protos.zodiac.StarSignRequest;
import resouces.protos.zodiac.StarSignResponse;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {
    private static final Logger logger = Logger.getLogger(Client.class.getName());

    private final StarSignServiceGrpc.StarSignServiceBlockingStub blockingStub;

    public Client(Channel channel) {
        blockingStub = StarSignServiceGrpc.newBlockingStub(channel);
    }

    public void starSign(String date) {
        logger.info("Will try to get " + date + " star sign.");
        StarSignRequest request = StarSignRequest
                .newBuilder()
                .setDate(date)
                .build();
        StarSignResponse response;
        try {
            response = blockingStub.getStarSign(request);
        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
            return;
        }
        logger.info("The star sign associated with the provided date is: " + response);
    }

    public static class DateValidator{
        private final DateTimeFormatter dateFormatter;

        public DateValidator(DateTimeFormatter dateFormatter) {
            this.dateFormatter = dateFormatter;
        }

        public boolean isValid(String dateStr) {
            LocalDate localDate;
            try {
                 localDate = LocalDate.parse(dateStr, this.dateFormatter);
            } catch (DateTimeParseException e) {
                return false;
            }
//            if (localDate.getMonthValue() == 2) {
//                if (localDate.getYear() % 4 == 0) {
//                    if (localDate.getDayOfMonth() > 29) {
//                        return false;
//                    }
//                }
//                else {
//                    if (localDate.getDayOfMonth() > 28) {
//                        return false;
//                    }
//                }
//            }
            return true;
        }
    }

    public static void main(String[] args) throws Exception {
        String date;
        String target = "localhost:50051";

        ManagedChannel channel = ManagedChannelBuilder.forTarget(target)
                .usePlaintext()
                .build();

        System.out.println("Introduceti data calendaristica: ");
        Scanner scanner = new Scanner(System.in);
        date = scanner.nextLine();

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        DateValidator validator = new DateValidator(dateTimeFormatter);

        try {
            Client client = new Client(channel);
            if (validator.isValid(date)) {
                client.starSign(date);
//                System.out.println("Valid date!");
            }
            else {
                System.out.println("Invalid data type. The accepted format: MM/dd/yyyy. Try again:");
            }
        } finally {
            channel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
        }
    }
}