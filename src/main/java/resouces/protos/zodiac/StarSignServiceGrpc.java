package resouces.protos.zodiac;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: zodiac.proto")
public final class StarSignServiceGrpc {

  private StarSignServiceGrpc() {}

  public static final String SERVICE_NAME = "zodiac.StarSignService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<resouces.protos.zodiac.StarSignRequest,
      resouces.protos.zodiac.StarSignResponse> getGetStarSignMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getStarSign",
      requestType = resouces.protos.zodiac.StarSignRequest.class,
      responseType = resouces.protos.zodiac.StarSignResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<resouces.protos.zodiac.StarSignRequest,
      resouces.protos.zodiac.StarSignResponse> getGetStarSignMethod() {
    io.grpc.MethodDescriptor<resouces.protos.zodiac.StarSignRequest, resouces.protos.zodiac.StarSignResponse> getGetStarSignMethod;
    if ((getGetStarSignMethod = StarSignServiceGrpc.getGetStarSignMethod) == null) {
      synchronized (StarSignServiceGrpc.class) {
        if ((getGetStarSignMethod = StarSignServiceGrpc.getGetStarSignMethod) == null) {
          StarSignServiceGrpc.getGetStarSignMethod = getGetStarSignMethod = 
              io.grpc.MethodDescriptor.<resouces.protos.zodiac.StarSignRequest, resouces.protos.zodiac.StarSignResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "zodiac.StarSignService", "getStarSign"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  resouces.protos.zodiac.StarSignRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  resouces.protos.zodiac.StarSignResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new StarSignServiceMethodDescriptorSupplier("getStarSign"))
                  .build();
          }
        }
     }
     return getGetStarSignMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static StarSignServiceStub newStub(io.grpc.Channel channel) {
    return new StarSignServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static StarSignServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new StarSignServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static StarSignServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new StarSignServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class StarSignServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void getStarSign(resouces.protos.zodiac.StarSignRequest request,
        io.grpc.stub.StreamObserver<resouces.protos.zodiac.StarSignResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetStarSignMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetStarSignMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                resouces.protos.zodiac.StarSignRequest,
                resouces.protos.zodiac.StarSignResponse>(
                  this, METHODID_GET_STAR_SIGN)))
          .build();
    }
  }

  /**
   */
  public static final class StarSignServiceStub extends io.grpc.stub.AbstractStub<StarSignServiceStub> {
    private StarSignServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private StarSignServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected StarSignServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new StarSignServiceStub(channel, callOptions);
    }

    /**
     */
    public void getStarSign(resouces.protos.zodiac.StarSignRequest request,
        io.grpc.stub.StreamObserver<resouces.protos.zodiac.StarSignResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetStarSignMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class StarSignServiceBlockingStub extends io.grpc.stub.AbstractStub<StarSignServiceBlockingStub> {
    private StarSignServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private StarSignServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected StarSignServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new StarSignServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public resouces.protos.zodiac.StarSignResponse getStarSign(resouces.protos.zodiac.StarSignRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetStarSignMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class StarSignServiceFutureStub extends io.grpc.stub.AbstractStub<StarSignServiceFutureStub> {
    private StarSignServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private StarSignServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected StarSignServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new StarSignServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<resouces.protos.zodiac.StarSignResponse> getStarSign(
        resouces.protos.zodiac.StarSignRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetStarSignMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_STAR_SIGN = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final StarSignServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(StarSignServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_STAR_SIGN:
          serviceImpl.getStarSign((resouces.protos.zodiac.StarSignRequest) request,
              (io.grpc.stub.StreamObserver<resouces.protos.zodiac.StarSignResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class StarSignServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    StarSignServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return resouces.protos.zodiac.Zodiac.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("StarSignService");
    }
  }

  private static final class StarSignServiceFileDescriptorSupplier
      extends StarSignServiceBaseDescriptorSupplier {
    StarSignServiceFileDescriptorSupplier() {}
  }

  private static final class StarSignServiceMethodDescriptorSupplier
      extends StarSignServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    StarSignServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (StarSignServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new StarSignServiceFileDescriptorSupplier())
              .addMethod(getGetStarSignMethod())
              .build();
        }
      }
    }
    return result;
  }
}
