// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: zodiac.proto

package resouces.protos.zodiac;

public interface StarSignResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:zodiac.StarSignResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string starSign = 1;</code>
   */
  java.lang.String getStarSign();
  /**
   * <code>string starSign = 1;</code>
   */
  com.google.protobuf.ByteString
      getStarSignBytes();
}
