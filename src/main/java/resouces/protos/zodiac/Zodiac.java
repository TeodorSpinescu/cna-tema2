// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: zodiac.proto

package resouces.protos.zodiac;

public final class Zodiac {
  private Zodiac() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_zodiac_StarSignRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_zodiac_StarSignRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_zodiac_StarSignResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_zodiac_StarSignResponse_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\014zodiac.proto\022\006zodiac\032\037google/protobuf/" +
      "timestamp.proto\"\037\n\017StarSignRequest\022\014\n\004da" +
      "te\030\001 \001(\t\"$\n\020StarSignResponse\022\020\n\010starSign" +
      "\030\001 \001(\t2U\n\017StarSignService\022B\n\013getStarSign" +
      "\022\027.zodiac.StarSignRequest\032\030.zodiac.StarS" +
      "ignResponse\"\000B&\n\026resouces.protos.zodiacB" +
      "\006ZodiacP\001\242\002\001Zb\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          com.google.protobuf.TimestampProto.getDescriptor(),
        }, assigner);
    internal_static_zodiac_StarSignRequest_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_zodiac_StarSignRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_zodiac_StarSignRequest_descriptor,
        new java.lang.String[] { "Date", });
    internal_static_zodiac_StarSignResponse_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_zodiac_StarSignResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_zodiac_StarSignResponse_descriptor,
        new java.lang.String[] { "StarSign", });
    com.google.protobuf.TimestampProto.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
